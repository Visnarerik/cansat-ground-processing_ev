/*
 * CancellationWatch.cpp
 *
 */

#include "CancellationWatch.h"
#include "CinManip.h"
#include "DebugUtils.h"
constexpr auto DBG=false;

using namespace std;

std::atomic<int> CancellationWatch::active={0};
std::thread * CancellationWatch::watchThread = {nullptr};
std::atomic<bool> CancellationWatch::threadRunning={false};
std::atomic<bool> CancellationWatch::cancelledByUser={false};
// About this syntax:
//  * T x = a is copy-initialization (temporary object t is created using
//    constructor accepting a, and x is initialized with the copy-constructor from t)
//    since std::atomic's copy constructor is deleted, this cannot be used here.
//  * T x = {a} is list-initialization (x is initialized with the constructor accepting a
//    as argument. No need for a copy-constructor: this works.
//

void CancellationWatch::start() {
	DBG_FCT
	active++;
	cancelledByUser=false;
	if (!threadRunning) {
		if (watchThread) delete watchThread;
		watchThread = new std::thread(CancellationWatch::watchThreadFunction);
		watchThread->detach();
	}
	// Only print invite if started for the first time
	if (active ==1) cout << "(Press Return to cancel processing)" << endl;
}

void CancellationWatch::stop() {
	DBG_FCT;
	active--;
	releaseThreadIfTerminated();
	// Do not clear cancelledByUser flag, so it can stil be read after stop.
	// If thread is still running, the thread object will remaing allocated (once).
	// Not great, but limited damage.
}

void CancellationWatch::releaseThreadIfTerminated() {
	if (!threadRunning) {
		if (watchThread) delete watchThread;
		watchThread=nullptr;
	}
}

bool CancellationWatch::cinDisturbed() {
	releaseThreadIfTerminated();
	return  (threadRunning.load()  && !(cancelledByUser.load()));
}


//  --------------------- Methods run in the watchThread only ---------------------------

/** Warning, this thread is detached and could be running after the object is deleted so, do not access
 *  it unless the static variable "active" is true.
 */
void CancellationWatch::watchThreadFunction() {
	DBG_FCT
	assert(threadRunning==false); // Never launch this thread twice!
	threadRunning=true;
	cin >> ignoreline();
	while (active.load()) {
		while (active.load() && !cancelledByUser.load()) {
			// Peek, etc. is a blocking call (ALL attempts to detected available input are):
			// the thread won't react until user presses return, possibly a very long time after
			// The object is inactive.
			// NB: reading with getch etc. is blocking.
			//     testing (cin.rdbuf()->in_avail())  does not work either : in_avail is allowed by
			//     standard to always return 0...
			//     get+putback works better than peek when the thread in unblocked by a console input
			//     intended for another thread. Although there is no guarantee it should work, it
			//     appears to work pretty good on MacOS at least.
			char c;
			c= (char) cin.get();
			cin.putback(c); // Try to restore the buffer in case others need it (no guarantee though,
							// since the other read is active in another thread and multithreading
							// cin produces undefined results. Works in some cases, though...
				// if not active, we'll just exit!
				if (active.load() ) {
					cin >> ignoreline();
					cout << "*** Do you want to cancel (y/n) ? ";
					string str;
					cin >> str;
					cin >> ignoreline();
					if ((str == "y") or (str=="Y")) {
						cancelledByUser=true;
					} else {
						cout << "Processing not cancelled (press Return to cancel). " << endl;
						this_thread::sleep_for(std::chrono::milliseconds(500));
					}
				}
			//} // peek()
		} // while active && not cancelled.
		// If still active, user cancelled.
		while (active.load() && cancelledByUser.load()) {
			// Sleep, unless the cancellation flag is reset or
			// the thread is stopped.
			this_thread::sleep_for(std::chrono::milliseconds(300));
		}
	} // while
	threadRunning=false;
} // watchThreadFunction

