/*
 * CSV_Row.h
 *
 * Source: https://stackoverflow.com/questions/1120140/how-can-i-read-and-parse-csv-files-in-c
 *
 * Usage:
 *  std::ifstream file("plop.csv");
 *  CSVRow row;
 *  while(file >> row)
 *  {
 *  	 std::cout << "4th Element(" << row[3] << ")\n";
 *  }
 *
 * Improvement A.Baudhuin, 2018/07:
 *   - now skip lines beginning with a '#' (comment lines).
 *   - added line number support.
 */

#pragma once

#include <vector>
#include <string>

class CSV_Row
{
    public:
		CSV_Row();
		CSV_Row(std::istream& str) : stream(&str){};
		/*! Method to call  before calling next(), if the object was created using the default constructor. */
		void setStream(std::istream& str) { stream = &str;} ;
        std::string const& operator[](std::size_t index) const  { return m_data[index]; }
        std::size_t size() const { return m_data.size(); }

        /*! read new row, skipping comments (lines starting with '#') and lines containing only blank characters..
         *  Expected use: while (row.next()) { process row };
         *  @param skipHeader If true, the first non empty line which does not start with a '#' character is skipped,
         *  	   assuming it is the header.
         *  @return True if a valid row was found
         */
        bool next(bool skipHeader=false);
        /*! Return the complete row */
        std::string getCompleteRow() const {return currentRow; } ;
        unsigned long getCurrentLine() const { return currentLineNumber;};
        /*! Skip 1 non-empty, non comment line in the stream.
         *  @return The number of lines actually skipped in the file (1 + number of comment lines + number of empty lines).
         */
        static unsigned int skipOneRow(std::istream &is);

        // Utility methods to perform numeric parsing.
        float getFloat(std::size_t idx) const {return std::stof(operator[](idx));} ;
        double getDouble(std::size_t idx) const {return std::stod(operator[](idx));} ;
        int getInt(std::size_t idx) const {return std::stoi(operator[](idx));} ;
        unsigned int getUInt(std::size_t idx) const {return (unsigned int) std::stoul(operator[](idx));} ;
        long getLong(std::size_t idx) const {return std::stol(operator[](idx));} ;
        unsigned long getULong(std::size_t idx) const {return std::stoul(operator[](idx));} ;
        bool getBool(std::size_t idx) const {return (std::stoi(operator[](idx)) != 0);} ;

        /*! Read next line and return float from first column */
        float getFloatFromNextLine();

    protected:
        /*! Find the next line containing data (i.e. non-empty, non-comment).
         *  @return True if a line is found, false otherwise.
         */
        bool readNextLineFromStream(bool skipHeader);

    private:
        std::string currentRow;		/*!< The whole row currently being processed */
        std::vector<std::string>    m_data;
        unsigned long currentLineNumber{};  /*!< current line number */
        std::istream* stream{};   /*!< The stream from which rows are retrieved */
};


