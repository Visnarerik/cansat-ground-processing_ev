/*
 * CalculationUtils.h
 *
 *  Created on: 30 juil. 2018
 *      Author: Alain
 */

#pragma once

#include <array>

void printDifferences(const std::array<float, 3> &a, const std::array<float,3> &b);

