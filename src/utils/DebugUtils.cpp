/*
 * DebugUtils.cpp
 *
 * See header file.
 */

#include "DebugUtils.h"

// Configure logging defaults:
void configureLogging() {
	el::Configurations defaultConf;
	defaultConf.setToDefault();
	// Values are always std::string
	defaultConf.set(el::Level::Debug,
			el::ConfigurationType::Format, "%msg  [%loc]");
	// default logger uses default configurations
	el::Loggers::reconfigureAllLoggers(defaultConf);
}




