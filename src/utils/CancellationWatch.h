/*
 * CancellationWatch.h
 *
 */

#pragma once
#include <atomic>
#include <thread>

/** @brief A utility class to watch the user input for cancellation during time-consuming processing.
 *
 *  This object is far from perfect, and a real implementation of this feature should use
 *  a console library like nCurses. The C++ console model has limitations which cannot be
 *  overcome.
 *  - All calls to cin are blocking. Therefore, sticking to standard C++ (no underlying OS calls),
 *    it is not possible to unblock a thread which has been peeking, getting or ready at cin in
 *    any way. As a consequence, there is no way to join such a thread, since it could take forever,
 *    would no input be received ever on cin.
 *  - The standard until C++11 at least does not provide any reliable mean to test for availability
 *    of data on cin without reading (and pending) on it.
 *  - Just clearing the buffer when something is received is quite a challenge (see ignore_line
 *    template function).
 *
 *  As a consequence, the following architecture is used, after many (many!) attempts to do better.
 *  - We use a single detached thread, managed through a number of static members and data members
 *    (this class is not zero-memory footprint when not used :-( )
 *  - The thread will exit whenever the CancellationWatch is stopped AND some input is received on
 *    cin. When the Cancellation watch is started, the existing thread will be reused, if any, or
 *    it will be launched again.
 *  - If the user actually cancels, everything is fine, the thread will exit when stop() is called,
 *    or at worse remain idle forever if it is not.
 *  - If the user does not cancel  before stop() is called, the thread will be blocked on cin, and
 *    will only exit the next time input is received on cin.
 *    This is not performed very nicely: this input will be received when another thred is expecting
 *    it, so cin will be accessed from 2 threads, which is VERY BAD. The minimal dammage is one of the
 *    following:
 *    	- If the CancellationWatch just peeks() at cin, it will result in a corruption of the first
 *    	  string read by the other process (typically, the first 2 characters are swapped).
 *    	- If the CancellationWatch just gets a character from cin, it will be lost to the other process,
 *    	  EVEN with a putback (the putback could arrive too late, and the character will most likely be
 *    	  read in a next call by the other process). Neverthess, calling putback immediately after
 *    	  get seems to do the trick (no reliability or portability expected...) at least on MacOS..
 *    	- If the CancellationWatch reads a string from cin, both processes could end up reading half
 *    	  the characters each from the buffer...
 *   The second options was selected, because at least for short inputs, it seems to work (for long strings,
 *   the putback will cause the first character to be retrieved in a next read).
 *
 *   The best we can do is provide a method that can be tested to check the default should be
 *   expected (cinDisturbed())
 *
 *  @par Usage
 *  @code
 *  {
 *     (...)
 *     CancellationWatch cw;
 *     while (!done && !cw.cancelled()) {
 *        // do time consuming stuff
 *     }
 *     if (cw.cancelled()) {
 *        // do whatever must be to react to cancellation rather than regular termination (optional)
 *     }
 *     (...)
 *  }
 *  @endcode
 *  Note that creation/destruction of several CancellationWatches is fully supported.
 */
class CancellationWatch {
public:
    /** constructor */
	CancellationWatch() { CancellationWatch::start(); };
	virtual ~CancellationWatch() { CancellationWatch::stop(); };

	static bool cancelled() { releaseThreadIfTerminated(); return cancelledByUser.load(); };
	static bool cinDisturbed();

protected:
	static void start();
	static void stop();
	static bool started() { releaseThreadIfTerminated(); return active.load(); };

	/** Method run by the watch thread. Do not call in the main thread */
	static void watchThreadFunction();
	/** Release the thread object if the thread is terminated. This method is called
	 *  whenever we get a chance.
	 */
	static void releaseThreadIfTerminated();
	static std::atomic<bool> cancelledByUser; /**< True if the user cancelled */
	static std::atomic<int> active;    	      /**< The number of active watches. If >0, and
												   the user did not cancel yet,
												   watch thread must keep running */
	static std::atomic<bool> threadRunning;   /**< true if the watch thread is running */
	static std::thread		 *watchThread;    /**< The thread watching for use input. */
};


