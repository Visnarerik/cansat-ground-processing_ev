/*
 * CSV_Record.cpp
 *
 *  Created on: 14 juil. 2018
 *      Author: Alain
 */

#include <CSV_Record.h>
#include <sstream>
#include <iomanip>
#include "DebugUtils.h"
constexpr auto DBG=false;

using namespace std;


CSV_Record::~CSV_Record() {
	DBG_FCT
	closeFiles();
}

CSV_Record & CSV_Record::operator=(const CSV_Record& other) {
	DBG_FCT
	if(this == &other) return *this;
	_numData=other._numData;
	_readOK=false;
	// Do not modify the file pointers, if any.
	// We obviously do not share the file pointers of other, but we'll keep our own, if any.
	return *this;
}

void CSV_Record::loadFromCSV_String(const std::string& recordAsCSV_String) {
	stringstream str(recordAsCSV_String);
	CSV_Row row(str);
	row.next();
	parseRow(row);
}

void CSV_Record::openInputFile(const std::string &filePath, bool skipHeader) {
	DBG_FCT
	assert(ifs == nullptr);
	ostringstream msg;
	ifs= new ifstream(filePath);
	if (!ifs->good()) {
		msg.clear();
		msg << "CSV_Record Cannot open input file '"<< filePath <<"'.";
		throw runtime_error(msg.str());
	};
	// Activate exceptions after first check, to throw an explicit msg
	ifs->exceptions( ifstream::failbit | ifstream::badbit );
	if (skipHeader) CSV_Row::skipOneRow(*ifs);
}

void CSV_Record::openOutputFile(const std::string &filePath, bool printHeader) {
	DBG_FCT
	LOG_IF(DBG, DEBUG) << "Opening output file '" << filePath << "'";
	assert(ofs == nullptr);
	ostringstream msg;
	ofs= new ofstream(filePath);
	if (!ofs->good()) {
		msg.clear();
		msg << "CSV_Record Cannot open output file '"<< filePath <<"'.";
		throw runtime_error(msg.str());
	};
	// Activate exceptions after first check, to throw an explicit msg
	ofs->exceptions( ofstream::failbit | ofstream::badbit );
	if (printHeader) printCSV_Header(*ofs);
}

void CSV_Record::write() {
	DBG_FCT
	assert(ofs);
	*ofs << endl;
	printCSV(*ofs);
}

bool CSV_Record::read() {
	DBG_FCT
	assert(ifs);
	readFromCSV(*ifs);
	return eof();
}

void CSV_Record::closeFiles() {
	DBG_FCT
	if (ifs) {
		ifs->close();
		ifs=nullptr;
	}
	if (ofs) {
		ofs->close();
		ofs=nullptr;
	}
}


void CSV_Record::readFromCSV(istream &is) {
	_readOK=false;
	ostringstream msg;
	CSV_Row row;

	if (!is.good()) {
		msg << "CSV_Record::readFromCSV: Input stream is not good. Aborted.";
		throw runtime_error(msg.str());
		return;
	}
	row.setStream(is);
	bool foundRow = row.next();
	if (!foundRow) {
		// Be silent, this is a condition routinely reach at end of file, if last lines
		// are empty.
		return;
	}
	if (_numData == 0) {
		// Take number of data items from first row.
		_numData = row.size();
	} else	if (row.size() != _numData) {
		msg << "CSV_Record::readFromCSV: A line should contain " << _numData << " values (found " << row.size()
							 <<"). Aborted." << endl;
		msg << "Parsing row '" << row.getCompleteRow() << "'" << endl;
		throw runtime_error(msg.str());
	}
	parseRow(row);
	_readOK=true;
}

void CSV_Record::printCSV_Array(	ostream &os,
								    const std::array<float, 3> &arr,
									bool asInt, bool finalSeparator)  const {
	 if (asInt) {
		 os <<  (int) arr[0] << separator <<  (int) arr[1] << separator <<  (int) arr[2] ;
	 } else {
		 os << std::fixed << setprecision(numFloatDecimalPositions) << arr[0] << separator <<arr[1] << separator << arr[2] ;
	 }
     if (finalSeparator) os << separator;
}

void CSV_Record::printCSV_Float(	ostream &os,
								const float f,
								bool finalSeparator) const {
	os << std::fixed << setprecision(numFloatDecimalPositions) << f;
	if (finalSeparator) os << separator;
}

void CSV_Record::readFromRow(const CSV_Row &row, unsigned int &index, std::array<float,3> &arr)  const {
	try {
		for (unsigned int i=0; i<3; i++) arr[i]=stof(row[index++]);
	}
	catch(const exception &e) {
		cout << "Exception parsing float from CSV file: found value '" << row[index] << "' at index "<<index << endl;
		cout << e.what() << endl;
		throw;
	}
}

void CSV_Record::readFromRow(const CSV_Row &row, unsigned int &index, float& f)  const {
	try {
		f=stof(row[index++]);
	}
	catch(const exception &e) {
		cout << "Exception parsing float from CSV file: found value '" << row[index] << "' at index "<<index << endl;
		cout << e.what() << endl;
		throw;
	}
}

void CSV_Record::readFromRow(const CSV_Row &row, unsigned int &index, unsigned long& n)  const {
	try {
		n=stoul(row[index++]);
	}
	catch(const exception &e) {
		cout << "Exception parsing unsigned long from CSV file: found value '" << row[index] << "' at index "<<index << endl;
		cout << e.what() << endl;
		throw;
	}
}


void CSV_Record::readFromRow(const CSV_Row &row, unsigned int &index, bool& b)  const {
	try {
		b=(stoi(row[index++]) != 0);
	}
	catch(const exception &e) {
		cout << "Exception parsing bool from file: found value '" << row[index] << "' at index "<<index << endl;
		cout << e.what() << endl;
		throw;
	}
}

ostream& operator<<(ostream& os, const CSV_Record & record) {
	record.printCSV(os);
	return os;
}

istream& operator>>(istream& is, CSV_Record & record) {
	record.readFromCSV(is);
	return is;
}
