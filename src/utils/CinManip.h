/*
 * CinManip.h
 *
 */

#pragma once
#include <iostream>

#include <ios>
#include <istream>
#include <limits>
#include <string>

/** @brief Utility function to clear the cin buffer.
 *  @author
 *
 *  This function makes a pretty subtle use of standard input and istreams to provide a reliable
 *  way to clear the input stream.
 *  @see https://www.daniweb.com/programming/software-development/threads/90228/flushing-the-input-stream
 *  @par Usage
 *  @code
 * 		clear_line(std::cin);
 * @endcode
 *
 *  21/8/18  Added support for message in pause.
 */
template <typename CharT>
std::streamsize ignore_line (
  std::basic_istream<CharT>& in, bool always_discard = false )
{
  std::streamsize nread = 0;

  if ( always_discard
    || ( in.rdbuf()->sungetc() != std::char_traits<CharT>::eof()
    && in.get() != in.widen ( '\n' ) ) )
  {
    // The stream is good, and we haven't
    // read a full line yet, so clear it out
    in.ignore ( std::numeric_limits<std::streamsize>::max(), in.widen ( '\n' ) );
    nread = in.gcount();
  }

  return nread;
}

/** @brief Utility manipulator to call ignore_line.
 *  @author
 *
 *  This classe makes allows using the ignore_line function through a manipulator.
 *  See also class pause, in this very file.
 *  @see https://www.daniweb.com/programming/software-development/threads/90228/flushing-the-input-stream
 *  @par Usage
 *  @code
 * 		std::cout<<"First input: ";
 * 		std::cin.get();
 * 		std::cout<<"Clearing cin.\n";
 * 		std::cin>> ignoreline();
 * 		std::cout<<"All done.\n";
 * 		std::cin>> pause();
 * @endcode
 */
class ignoreline {
  bool _always_discard;
  mutable std::streamsize _nread;
public:
  ignoreline ( bool always_discard = false )
    : _always_discard ( always_discard ), _nread ( 0 )
  {}

  std::streamsize gcount() const { return _nread; }

  template <typename CharT>
  friend std::basic_istream<CharT>& operator>> (
    std::basic_istream<CharT>& in, const ignoreline& manip )
  {
    manip._nread = ignore_line ( in, manip._always_discard );
    return in;
  }
};

/** @brief Utility manipulator to wait until the user hits return in the console
 *  @author
 *
 *  This classe makes a pretty subtle use of standard input and istreams to provide a reliable
 *  way to wait for the next return
 *  See also class ignore, in this very file.
 *  @see https://www.daniweb.com/programming/software-development/threads/90228/flushing-the-input-stream
 *  @par Usage
 *  @code
 * 		std::cout<<"First input: ";
 * 		std::cin.get();
 * 		std::cout<<"Clearing cin.\n";
 * 		std::cin>> ignoreline();
 * 		std::cout<<"All done.\n";
 * 		std::cin>> pause();
 *  @endcode
 */
class pause {
  ignoreline _ignore;
  const std::string &_msg;
public:
  pause (const std::string &msg="", bool always_discard = false )
    : _ignore ( always_discard ), _msg(msg)
  {}

  std::streamsize gcount() const { return _ignore.gcount(); }

  template <typename CharT>
  friend std::basic_istream<CharT>& operator>> (
    std::basic_istream<CharT>& in, const pause& manip )
  {
    if ( !( in>> manip._ignore ) )
      in.clear();

    if (manip._msg.empty()) {
    	std::cout<<"Press Enter to continue . . .";
    } else {
    	std::cout << manip._msg;
    }

    return in.ignore();
  }
};



