/*
 * DbgFunction.h
 *
 *  Created on: 9 juin 2018
 *      Author: Alain
 */

#pragma once
#ifdef USE_DBG_FUNCTION
#include "easylogging++.h"
#include <iostream>
#include <iomanip>
#include <string>

#define DBG_FCT DbgFunction dbgfct(DBG,__PRETTY_FUNCTION__);

class DbgFunction {
public:
	DbgFunction(bool isActive, const char * functionName) : fctName(functionName) , active(isActive) {
		if (active) {
			if (depth==-1) {
				el::Loggers::getLogger("dbgFunction");
				// Configure our logger:
				el::Configurations defaultConf;
				defaultConf.setToDefault();
				// Values are always std::string
				defaultConf.set(el::Level::Debug,
						el::ConfigurationType::Format, "%msg");
				// default logger uses default configurations
				el::Loggers::reconfigureLogger("dbgFunction", defaultConf);
				depth=0;
			}
			CLOG(DEBUG,"dbgFunction") << std::setfill(indentChar) << std::setw(2*depth) << "" << depth+1 << ". -->" << functionName;
			depth++;
		}
	};
	virtual ~DbgFunction() {
		if (active) {
			depth--;
			CLOG(DEBUG, "dbgFunction") << std::setfill(indentChar) << std::setw(2*depth) << "" << depth+1 << ". <--" << fctName;
		}
	}
protected:
	static int depth;
	static const char indentChar=' ';
	std::string fctName;
	bool active;
};

#else
#define DBG_FCT
#endif
