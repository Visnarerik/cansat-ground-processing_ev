/*
 * SerialPortListener.cpp
 *
 *  Created on: 18 juil. 2018
 */

#include "SerialPortListener.h"
#include <iostream>
#include <exception>
#include "DebugUtils.h"
#include "TrimStdString.h"

using namespace std;

SerialPortListener::SerialPortListener(	Processor& theProcessor,
										uint32_t baudRate,
										bool limitToUSB_Ports) :
 RecordSource(theProcessor, withVisualFeedback),
 theSerial("", baudRate,serial::Timeout::simpleTimeout(1000)),
 thePort(""),
 limitToUSB_Ports(limitToUSB_Ports ){

}

SerialPortListener::SerialPortListener(	uint32_t baudRate,
										bool limitToUSB_Ports) :
 RecordSource(withVisualFeedback),
 theSerial("", baudRate,serial::Timeout::simpleTimeout(1000)),
 thePort(""),
 limitToUSB_Ports(limitToUSB_Ports ){

}

SerialPortListener::~SerialPortListener() {
	// TODO Auto-generated destrquctor stub
}

bool SerialPortListener::prepare() {
	thePort = selectPort(limitToUSB_Ports);
	thePort.empty(); return false;

	bool result=false;
	try {
		cout << "Opening port '" << thePort << "' at " << theSerial.getBaudrate() << " bauds ..." << flush;
		theSerial.setPort(thePort);
		theSerial.open();
		if (theSerial.isOpen()){
			cout << "OK" << endl << flush;
			result=true;
		} else {
			cout << "Error: cannot open port '" << thePort << "'" << endl;
		}

	} // try
	catch(exception &e) {
		cout << endl << "*** Error: " << e.what() << endl;
	}// catch
	return result;
}

void SerialPortListener::restrictToUSB_Ports(vector<serial::PortInfo>& myPorts) {
	DBG_FCT
#ifdef _WIN32
	cout << ("Filtering usb ports not implemented for Windows (TODO)");
	return;
#endif

	// macOS, linux
	vector<serial::PortInfo>::iterator it = myPorts.begin();
	while (it < myPorts.end()) {
		LOG_IF(DBG,DEBUG) << "..." << it->port;
		if (it->port.find("usb") == std::string::npos) {
			LOG_IF(DBG,DEBUG) << "Suppressing " << it->port << "(descr="
					<< it->description << ", hw=" << it->hardware_id << ")";
			myPorts.erase(it);
		} else {
			LOG_IF(DBG,DEBUG) << "Retained: " << it->port << "(descr="
					<< it->description << ", hw=" << it->hardware_id << ")";
			it++;
		}
	} // while
}

void SerialPortListener::useTTYandNotCU(vector<serial::PortInfo>& thePorts) {
	DBG_FCT
	for (vector<serial::PortInfo>::iterator it = thePorts.begin();it < thePorts.end(); it++) {
		if (it->port.find("/dev/cu.") == 0) {
			LOG_IF(DBG,DEBUG) << "Replacing '/dev/cu.' in " << it->port;
			it->port.replace(0, 8, "/dev/tty.");
			LOG_IF(DBG,DEBUG) << "Done: " << it->port;
		}
	}
}

std::string SerialPortListener::selectPort(bool limitToUSB_Ports) {
	DBG_FCT
	vector<serial::PortInfo> myPorts = serial::list_ports();
	LOG_IF(DBG,DEBUG) << "Found " << myPorts.size() << " serial port(s)";

	// Limit to USB
	if(limitToUSB_Ports) restrictToUSB_Ports(myPorts);

#ifndef _WIN32
	// replace /dev/cu.xxxx with /dev/tty.xxxx On MacOS at least, this is a must
    // see https://stackoverflow.com/questions/8632586/macos-whats-the-difference-between-dev-tty-and-dev-cu
	useTTYandNotCU(myPorts);
#endif

    // Finally consider the ports we have...
	LOG_IF(DBG,DEBUG) << "Retained " << myPorts.size() << " serial port(s)";
	if (myPorts.empty()) {
		cout << "No serial port available." << endl;
		return "";
	}
	if (myPorts.size()==1) return myPorts.at(0).port;

	// TODO: this should use a general-purpose utility function getChoice (...)
	bool done=false;
	int choice;
	uint32_t counter=0;
	string str;
	while (!done) {
		counter=0;
		cout << "Select a port to listen to and press return:" << endl;
		for (auto port : myPorts) {
			counter++;
			cout <<  counter << ". " << port.port << " (descr.: " << port.description << ", hw: " << port.hardware_id<< ")"<<endl << flush;
		}
		try {
			str="";
			while (str.empty()) {
				cout << "> " ;
				getline(cin, str);
			}
			choice = stoi(str);
			LOG_IF(DBG, DEBUG)  << "Got str="<< str << ", choice=" << choice << ")" << flush;
			//while (ch != '\n') cin.get(ch); // swallow until '\n' include
			if ( (choice > 0) && (choice <= (int) myPorts.size()) ) done =true;
			else cout << "Error: invalid value "<< choice << "): expected 0..." << (int) myPorts.size() << endl;
		}
		catch (exception &e) {
			cout << "Error: invalid value '"<< str << "': try again!" << endl;
		}

	} // while
	LOG_IF(DBG, DEBUG) << "Valid choice: " << choice << ", returning '"<< myPorts[(unsigned int) choice-1].port << "'";
	return myPorts[(unsigned int) choice-1].port;
}

bool SerialPortListener::feedOneRecordToProcessor(Processor &theProcessor, bool &success) {
	DBG_FCT
	success=false;

	// Not really clear whether we properly handle timeout if the serial line is broken...
	// TO BE TESTED.
	try {
		string stringBuffer;

		// For some reason I don't understand, the following line does not work....
		// size_t read=theSerial.readline(stringBuffer);
		stringBuffer=theSerial.readline();
		//remove final "\n"
		if (!stringBuffer.empty()) stringBuffer.pop_back();
		trim(stringBuffer);
		// filter out empty lines
		if (!stringBuffer.empty()) {
			success=theProcessor.processString(stringBuffer);
		}

	} // try
	catch(exception &e) {
		cout << endl << "*** Error: " << e.what() << endl;
	}// catch
	return true;
}

void SerialPortListener::testEcho() {
	serial::Serial theSerial("/dev/tty.usbmodem1411", 115200, serial::Timeout::simpleTimeout(1000));

	try {
		cout << "Opening port '" << theSerial.getPort() << "' at " << theSerial.getBaudrate() << " bauds ..." << flush;
		if (theSerial.isOpen()){
			cout << "OK" << endl << flush;
		} else {
			cout << "Error: cannot open port "<< endl;
		}

		string stringBuffer;
		while (1) {
			stringBuffer=theSerial.readline();
			cout<< stringBuffer.length() << " chars: " << stringBuffer;
		}
	} catch (exception &e) {
		cout << "Exception: " << e.what() << endl;
	}
	theSerial.close();
}
