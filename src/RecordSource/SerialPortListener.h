/*
 * SerialPortListener.h
 *
 */

#pragma once
#include "serial/serial.h"
#include "RecordProcessor/RecordProcessor.h"
#include "RecordSource/RecordSource.h"

/** A record source feebing processors from strings received on a serial port.
 *
 *  The available ports are scanned, if more than one is available, the user is prompted for selection.
 *
 *  TO BE COMPLETED: BEHAVIOUR WHEN LINK SEVERED ?
 */
class SerialPortListener : public RecordSource {
public:
	/** Constructor
	 * @param theProcessor The object responsible for processing every line of data received from the port.
	 * @param baudRate  The baud rate for listening to the port.
	 * @param limitToUSB_Ports If true, only USB serial ports are considered (this feature currently
	 *  	  only works on MacOS and Linux. To be completed for Windows.
	 */
	SerialPortListener(Processor &theProcessor, uint32_t baudRate, bool limitToUSB_Ports=true);

	/** Constructor.
	 * 	@warning When using this constructor, be sure to call #setProcessor() or #appendProcessor()
	 *  		 before running the source.
	 *  @param baudRate  The baud rate for listening to the port.
	 *  @param limitToUSB_Ports If true, only USB serial ports are considered (this feature currently
	 *  	   only works on MacOS and Linux. To be completed for Windows.
	 */
	SerialPortListener(uint32_t baudRate, bool limitToUSB_Ports=true);
	virtual ~SerialPortListener();

	/** This is a test method, used during development only. It just echoes the data received from an
	 *  hardcoded port on cout
	 */
	static void testEcho();

protected:
	/** @name Overloaded methods from base class
	 *  Those methods are defined by RecordSource.
	 *  @{
	 */

	/** Select the port to listen to.
	 *  @return true if preparation was successful and the record source is ready
	 *          to feed the processor, false otherwise.
	 */
	virtual bool prepare();

	/** Consume data from the port line by line, and pass each line to the processor for appropriate action.
	 *  TODO: clarify in which condition this method return false ???
	 *  @param  processor The object responsible for processing every line of data received from the port.
	 *  @param  success This parameter is updated to true by the subclass if the processing of
	 *  	            the record was successful, and to false otherwise.
	 *  @return true if the method should be called again (because there is data left to process)
	 *          false if the last record has been processed or some error make it impossible
	 *          to process additional records.
	 */
	virtual bool feedOneRecordToProcessor(Processor &theProcessor, bool &success);
    /** @} */ // End of group of overloaded methods

    //----------------------------------------------------------------------------------------------------

	/** Lists all available ports on cout, allow the user to select one and returns the string
	 *  to be used to open the port. If only 1 port is available for selection, it is
	 *  returned without interaction with the user. If none is available, an empty string is
	 *  returned.
	 *  @param limitToUSB_Ports If true, only USB serial ports are considered (this feature currently
	 *  		 only works on MacOS and Linux. To be completed for Windows).
	 */
	static std::string selectPort(bool limitToUSB_Ports=true);

	/** Filter vector to keep only USB serial ports. Currently operational on Linux/MacOS only.
	 *  @param thePorts The port vector to filter
	 */
	static void restrictToUSB_Ports(std::vector<serial::PortInfo>& thePorts);

	/** Transform all "/dev/cu.*" ports into "/dev/tty.*" On MacOS at least, this is a must:
     *  see https://stackoverflow.com/questions/8632586/macos-whats-the-difference-between-dev-tty-and-dev-cu
	 *  @param thePorts The vector of ports to process
	 */
	static void useTTYandNotCU(std::__1::vector<serial::PortInfo>& thePorts);

private:
	serial::Serial theSerial;
	std::string thePort;
	bool limitToUSB_Ports;
	static constexpr auto DBG=false;
};
