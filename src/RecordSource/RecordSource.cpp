/*
 * RecordSource.cpp
 */

#include "RecordSource/RecordSource.h"
#include "DebugUtils.h"

uint32_t RecordSource::feedProcessor(allowUserInterrupt_t userInterrupt) {
	DBG_FCT
	bool success=false;
	uint32_t count=0;
	if (userInterrupt && (!cancellationWatch)) cancellationWatch=new CancellationWatch();
	while (!cancellationWatch->cancelled() && feedOneRecordToProcessor(*processor, success)){
		if (success) {
			count++;
		}
		else {
			std::cout << "*** Error processing record! *** " << std::endl;
		}
		if (visualFeedback && ((count % 100L)==0L)) {
			std::cout << "." << std::flush;
		}
	}
	if (userInterrupt && (cancellationWatch)) {
		delete cancellationWatch;
		cancellationWatch=nullptr;
	}
	// Do not worry about exceptions: the destructor will stop it
	                           // anyway.
	std::cout << std::endl << "Processed " << count  << " records."<< std::endl;
	return count;
}

uint32_t RecordSource::run(allowUserInterrupt_t userInterrupt){
  	DBG_FCT
		if (processor == nullptr) {
			throw runtime_error("*** RecordSource::run() called without a processor");
		}

  	uint32_t  num=0;
  	if (prepare()) {
  		num = feedProcessor(userInterrupt);
  		processor->terminateProcessing();
  		terminate();
  	} else {
  		std::cout << "Error during preparation of record source (aborted)." << std::endl;
  	}
  	return num;
  }

void RecordSource::printProcessorChainDescription(ostream& os) const {
	os << "RecordSource: " << typeid(*this).name() << ": ";
	if (processor) {
		os << endl;
		processor->printProcessorChainDescription(os);
	} else os << "No processor attached." << endl;
}
