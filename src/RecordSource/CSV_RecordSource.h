/*
 * CSV_RecordSource.h
 *
 */

#pragma once
#include <fstream>

#include "RecordSource/RecordSource.h"
#include "CSV_Row.h"
#include "DebugUtils.h"


/** @brief An object feeding a RecordProcessor with CSV_Records (as strings) from a file.
 */
class CSV_RecordSource: public RecordSource {
public:
	/** Constructor.
	 *  The CSV_RecordSource must be provided with a processor to feed, a valid path to an
	 *  existing and readable file containing CSV records
	 *  @param theProcessor The processor to feed with strings to parse into CSV_Records.
	 *  @param inputFilePath The file to retrieve records from. It is assumed to include a header.
	 *  @param visualFeedback If true, a character is output on cout every 100 records, to make
	 *						  processing visible
	 */
	CSV_RecordSource(		Processor &theProcessor,
							const std::string &inputFilePath,
							visualFeedback_t visualFeedback=withVisualFeedback):
		RecordSource(theProcessor, visualFeedback),
		filePath(inputFilePath){
			DBG_FCT
			is.exceptions(ifstream::badbit | ifstream::failbit);
	};

	/** Constructor.
	 *  The CSV_RecordSource must be provided with a processor to feed, a valid path to an
	 *  existing and readable file containing CSV records.
	 *  @warning When using this constructor, be sure to call #setProcessor() or #appendProcessor()
	 *  		 before running the source.
	 *  @param inputFilePath The file to retrieve records from. It is assumed to include a header.
	 *  @param visualFeedback If true, a character is output on cout every 100 records, to make
	 *						  processing visible
	 */
	CSV_RecordSource(const std::string &inputFilePath,
					 visualFeedback_t visualFeedback=withVisualFeedback):
		RecordSource(visualFeedback),
		filePath(inputFilePath){
			DBG_FCT
			is.exceptions(ifstream::badbit | ifstream::failbit);
	};
	virtual ~CSV_RecordSource() {};

protected:
	/** @name Overriden methods from base class
	 *  Those methods are defined by RecordSource.
	 *  @{
	 */

	/** Open the input file, skip header.
	 *  @return true if the file exists and can be opened, false otherwise.
	 */
	virtual bool prepare(){
		DBG_FCT
		bool result = false;
		try {
			is.open(filePath);
			row.setStream(is);
			row.skipOneRow(is);
			cout << "Processing file '" << filePath << "'..." << endl;
			result=!is.eof();
		} catch(exception &e) {
			cout << e.what() << endl;
			// swallow exception.
		}
		return (result);
	};

	/** Consume 1 line of data, and pass record to the processor for appropriate action.
	 *  @param  success This parameter is updated to true if the processing of
	 *  	            the record was successful, and to false otherwise.
	 *  @param  processor The object responsible for processing every line of data received from the port.
	 *  @return true if the method should be called again (because there is data left to process)
	 *          false if the last record has been processed or some error make it impossible
	 *          to process additional records.
	 */
	virtual bool feedOneRecordToProcessor(Processor &processor, bool &success) {
		DBG_FCT
		bool recordsLeft = false;
		if (row.next(false)) {
			success=processor.processString(row.getCompleteRow());
			recordsLeft=true;
		}
		return recordsLeft;
	};

    /** @} */ // End of group of overloaded methods


private:
	std::string filePath;    /**< Full path tot the file from which CSV_Records are read */
	CSV_Row  row;	 /**< The record used read the file and extract only meaningfull lines */
	std::ifstream is;	 /**< The stream records are read from */
	static constexpr auto DBG=false;
};

