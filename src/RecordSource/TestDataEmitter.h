/*
 * TestDataEmitter.h
 *
 *  Created on: 18 juil. 2018
 */

#pragma once
#include <RecordProcessor/Processor.h>
#include "RecordSource/RecordSource.h"

#include <sstream> // for cpp
#include <iostream>
#include <string>
#include <unistd.h> // usleep.

/** A small utility class which generates meaningless strings and provides them to the processor. It can easily
 *  be used as a substitute to a SerialPortListener for the purpose of testing the communication chain.
 */
class TestDataEmitter : public RecordSource {
public:
	TestDataEmitter(uint32_t numStrings, uint32_t delayInMsec, Processor& theProcessor)
		: RecordSource(theProcessor, withVisualFeedback),
		  delayInMicrosecs(delayInMsec*1000),
		  numStrings(numStrings),
		  count(0) {} ;
	virtual ~TestDataEmitter() {};
protected:
	virtual bool feedOneRecordToProcessor(Processor& processor, bool &success)
	{
		count++;
		ostringstream str;
		str << "String #" << count << '/' << numStrings;
		success=processor.processString(str.str());
		if (!success) cout << "*** Error processing the " << count <<"th string" << endl;
		usleep(delayInMicrosecs);
		return (count < numStrings);
	}

private:
	uint32_t delayInMicrosecs;   /*< Delay between strings in usec */
	uint32_t numStrings;
	uint32_t count;
};
