/*
 *  xxxxRecord.h
 *
 *  A DataSet containing all data obtained from the CanSat except images.
 *  Units: acceleration in m/s^2, gyro in rad/s, magnetometer in µT.
 *
 */

#pragma once

#include <iostream>
#include <array>
#include "CSV_Row.h"
#include "CSV_Record.h"

using namespace std;
using namespace std::chrono;

//#define INCLUDE_MAG_RAW_CALIBRATED // Previous version included an additional 3 data

class xxxxRecord  : public CSV_Record {
public:
	xxxxRecord();
	xxxxRecord(const xxxxRecord&)=delete;
	virtual ~xxxxRecord();

	// Public data members carried by the record
	unsigned long timestamp{}; // in msec.
	// A. IMU data
	array<float, 3> IMU_AccelRaw{}, IMU_Accel{};
	array<float, 3> gyroRaw{}, gyro{};
#ifdef INCLUDE_MAG_RAW_CALIBRATED
	array<float, 3> magCalib{};
#endif
	array<float, 3> magRaw{}, mag{};
	// B. GPS data
	bool  newGPS_Measures{};			/**< true if GPS data is included in the record */
	float GPS_LatitudeDegrees{};		/**< The latitude in decimal degrees, ([-90;90], + = N, - = S) */
	float GPS_LongitudeDegrees{};     /**< The longitude in decimal degrees ([-180;180], + =E, - =W) */
	float GPS_Altitude{};				/**< Altitude of antenna, in meters above mean sea level (geoid) */
	float GPS_VelocityKnots{};		/**< velocity over ground in Knots (1 knot = 0.5144447 m/s) */
	float GPS_VelocityAngleDegrees{}; /**< Direction of velocity in decimal degrees, 0 = North */

	// C. AHRS data
	float AHRS_AccelX{}, AHRS_AccelY{}, AHRS_AccelZ{};
	float roll{}, yaw{}, pitch{};
	// D. Primary mission data
	float temperature{};
    float pressure{};
    float altitude{};
	// E. Secondary mission data
	// TODO: completed secundary mission data
    float data1{};
    float data2{};

	// set all data to 0 or ""
	virtual void clear();
	// setReferenceTime to current time.
	virtual void initReferenceTime();
	// Update timestamp, relative to referenceTime
	virtual void updateTimeStamp();
	virtual void printCSV_Header(ostream &os) const;
	virtual void printCSV(ostream &os) const;
	virtual void printCalibrationDifferences(const xxxxRecord &other) const;

protected:
	/** Constructor.
	 * @param numExtraElements The number of data elements added by the subclass
	 */
	xxxxRecord(unsigned int numExtraElements);
	virtual unsigned int parseRow(const CSV_Row &row);
	high_resolution_clock::time_point refTimePoint;
    friend ostream & operator<<(ostream &os, const xxxxRecord & record);
    friend istream& operator>>(istream& is,  xxxxRecord & record);
};

ostream & operator<<(ostream &os, const xxxxRecord & record);
istream& operator>>(istream& is,  xxxxRecord & record);


