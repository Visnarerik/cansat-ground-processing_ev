/*
 * xxxxGroundRecord.cpp
 *
 *  Created on: 30 juil. 2018
 *      Author: Alain
 */

#include "xxxxGroundRecord.h"
#include "DebugUtils.h"
//constexpr auto DBG=false;

xxxxGroundRecord::xxxxGroundRecord(): xxxxRecord(12) {}

xxxxGroundRecord& xxxxGroundRecord::operator=(const xxxxRecord& other){
	if (this == &other) return *this;
	xxxxRecord::operator=(other);
	return *this;
}
xxxxGroundRecord::~xxxxGroundRecord() {
	// TODO Auto-generated destructor stub
}

void xxxxGroundRecord::clear() {
	xxxxRecord::clear();
	GPS_PositionX=GPS_PositionY=GPS_PositionZ=0.0f;
	GPS_VelocityX=GPS_VelocityY=GPS_VelocityZ=0.0f;
	positionX=positionY=positionZ=0.0;
	velocityX=velocityY=velocityZ=0.0;
}

void xxxxGroundRecord::printCSV_Header(ostream &os) const {
	xxxxRecord::printCSV_Header(os);
  os << ", GPS_PosX, GPS_PosY, GPS_PosZ, GPS_VelocityX, GPS_VelocityY, GPS_VelocityZ, "
     << "posX, posY, posZ, velocityX, velocityY, velocityZ";
}

void xxxxGroundRecord::printCSV(ostream &os) const {
	xxxxRecord::printCSV(os);
	os << CSV_Record::separator;
	os << GPS_PositionX << CSV_Record::separator << GPS_PositionY << CSV_Record::separator << GPS_PositionZ << CSV_Record::separator;
	os << GPS_VelocityX << CSV_Record::separator << GPS_VelocityY << CSV_Record::separator << GPS_VelocityZ << CSV_Record::separator;

	os << positionX << CSV_Record::separator << positionY << CSV_Record::separator << positionZ << CSV_Record::separator;
	os << velocityX << CSV_Record::separator << velocityY << CSV_Record::separator << velocityZ;
	// No separator after last value.
}

unsigned int xxxxGroundRecord::parseRow(const CSV_Row &row) {
	unsigned int idx = xxxxRecord::parseRow(row);
	readFromRow(row, idx, GPS_PositionX);
	readFromRow(row, idx, GPS_PositionY);
	readFromRow(row, idx, GPS_PositionZ);
	readFromRow(row, idx, GPS_VelocityX);
	readFromRow(row, idx, GPS_VelocityY);
	readFromRow(row, idx, GPS_VelocityZ);

	readFromRow(row, idx, positionX);
	readFromRow(row, idx, positionY);
	readFromRow(row, idx, positionZ);
	readFromRow(row, idx, velocityX);
	readFromRow(row, idx, velocityY);
	readFromRow(row, idx, velocityZ);
	// No need to skip rest of line. This is taken care of by CSV_Row
	return idx;

}
