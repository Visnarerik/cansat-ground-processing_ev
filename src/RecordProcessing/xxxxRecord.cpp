/*
 * xxxxRecord.cpp
 *
 *  Created on: 7 juin 2018
 */

#include <cmath>
#include <iomanip>
#include "xxxxRecord.h"
#include "CSV_Row.h"
#include "CalculationUtils.h"
#include "DebugUtils.h"
#include "IsaTwoInterface.h"
constexpr auto DBG=false;
constexpr unsigned char recordType=0; // TODO: take from interface file ?

#ifdef INCLUDE_MAG_RAW_CALIBRATED
constexpr auto numDataElements=39;
#else
constexpr auto numDataElements=36;
#endif

xxxxRecord::xxxxRecord() : xxxxRecord(0) {};

xxxxRecord::xxxxRecord(unsigned int numExtraElements) : CSV_Record(numDataElements+numExtraElements) {
	initReferenceTime();
	clear();
}

void xxxxRecord::clear() {
	DBG_FCT
	timestamp=0L;
	std::fill(IMU_AccelRaw.begin(), IMU_AccelRaw.end(), 0.0f);
	std::fill(IMU_Accel.begin(), IMU_Accel.end(), 0.0f);
	std::fill(magRaw.begin(), magRaw.end(), 0.0f);
#ifdef INCLUDE_MAG_RAW_CALIBRATED
	std::fill(magCalib.begin(), magCalib.end(), 0.0f);
#endif
	std::fill(mag.begin(), mag.end(), 0.0f);
	std::fill(gyroRaw.begin(), gyroRaw.end(), 0.0f);
	std::fill(gyro.begin(), gyro.end(), 0.0f);
	// GPS data
	newGPS_Measures=false;
	GPS_LatitudeDegrees = GPS_LongitudeDegrees = 0.0f;
	GPS_VelocityKnots = GPS_VelocityAngleDegrees = 0.0f;
	GPS_Altitude = 0.0f;
	// AHRS
	AHRS_AccelX=AHRS_AccelY=AHRS_AccelZ=0.0f;
	roll=yaw=pitch=0.0f;
	// Primary mission
	temperature=pressure=altitude= 0.0f;
	// Secundary mission
	data1=data2=0.0f;
}

xxxxRecord::~xxxxRecord() {}

void xxxxRecord::printCSV_Header(ostream &os) const {
  os << "type, timestamp(ms),accel. raw X, accel. raw Y, accel. raw Z,"
	 << "IMU accel. m/s^2 X, IMU accel. m/s^2 Y, IMU accel. m/s^2 Z,"
	 << "mag. raw X, mag. raw Y, mag. raw Z, "
#ifdef INCLUDE_MAG_RAW_CALIBRATED
	 << "mag. calib X, mag. calib Y, mag. calib Z,"
#endif
	 << "mag. (µT) X, mag. (µT) Y, mag. (µT) Z,"
	 << "gyro raw X, gyro raw Y, gyro raw Z, "
	 << "gyro (rps) X, gyro (rps) Y, gyro (rps) Z, ";
  // GPS
  os << "GPS present (0/1), GPS Lat degrees, GPS Long degrees, "
     << "GPS alt, GPS Velocity knots, GPS Velocity degrees, ";
  // AHRS
  os << "AHRS accel m/s^2 X, AHRS accel m/s^2 Y, AHRS accel m/s^2 Z, "
     << "Roll, Yaw, Pitch, ";
  // Primary mission
  os << "Temp, Pressure, Altitude, ";
  // Secundary mission
  os << "data1, data2";
}


void xxxxRecord::printCSV(ostream &os) const {
	os << (int) IsaTwoRecordType::DataRecord << separator << timestamp << separator;
	// A. IMU data
	printCSV_Array(os, IMU_AccelRaw, true, true);
	printCSV_Array(os, IMU_Accel, false, true);
	printCSV_Array(os, magRaw, true, true);
#ifdef INCLUDE_MAG_RAW_CALIBRATED
	printCSV_Array(os, magCalib, false, true);
#endif
	printCSV_Array(os, mag, false, true);
	printCSV_Array(os, gyroRaw, false, true);
	printCSV_Array(os, gyro, false, true);

	// B. GPS Data
	os << newGPS_Measures << separator;
	printCSV_Float(os, GPS_LatitudeDegrees, true);
	printCSV_Float(os, GPS_LongitudeDegrees, true);
	printCSV_Float(os, GPS_Altitude, true);
	printCSV_Float(os, GPS_VelocityKnots, true);
	printCSV_Float(os, GPS_VelocityAngleDegrees, true);

	// C. AHRS data
	printCSV_Float(os, AHRS_AccelX, true);
	printCSV_Float(os, AHRS_AccelY, true);
	printCSV_Float(os, AHRS_AccelZ, true);
	printCSV_Float(os, roll, true);
	printCSV_Float(os, yaw, true);
	printCSV_Float(os, pitch, true);

	// D. Primary mission
	printCSV_Float(os, temperature, true);
	printCSV_Float(os, pressure, true);
	printCSV_Float(os, altitude, true);

	// D. Secundary mission
	printCSV_Float(os, data1, true);
	printCSV_Float(os, data2, false);

	// No separator after last value.
}

unsigned int xxxxRecord::parseRow(const CSV_Row &row) {
	unsigned int idx=0;
	unsigned long recordType;
	try {
		readFromRow(row, idx, recordType);
		readFromRow(row, idx, timestamp);
		// A. IMU data
		readFromRow(row, idx, IMU_AccelRaw);
		readFromRow(row, idx, IMU_Accel);
		readFromRow(row, idx, magRaw);
#ifdef INCLUDE_MAG_RAW_CALIBRATED
		readFromRow(row, idx, magCalib);
#endif
		readFromRow(row, idx, mag);
		readFromRow(row, idx, gyroRaw);
		readFromRow(row, idx, gyro);

		// B. GPS data
		readFromRow(row,idx,  newGPS_Measures);
		readFromRow(row,idx,  GPS_LatitudeDegrees);
		readFromRow(row,idx,  GPS_LongitudeDegrees);
		readFromRow(row,idx,  GPS_Altitude);
		readFromRow(row,idx,  GPS_VelocityKnots);
		readFromRow(row,idx,  GPS_VelocityAngleDegrees);

		// C. AHRS data
		readFromRow(row,idx,  AHRS_AccelX);
		readFromRow(row,idx,  AHRS_AccelY);
		readFromRow(row,idx,  AHRS_AccelZ);
		readFromRow(row,idx,  roll);
		readFromRow(row,idx,  yaw);
		readFromRow(row,idx,  pitch);

		// D. Primary mission
		readFromRow(row,idx,  temperature);
		readFromRow(row,idx,  pressure);
		readFromRow(row,idx,  altitude);

		//E. Secundary mission
		readFromRow(row,idx,  data1);
		readFromRow(row,idx,  data2);

		return idx;
	}
	catch(const exception &e) {
		cout << "Exception parsing IMU file: found value '" << row[idx] << "' at index "<<idx << endl;
		cout << e.what() << endl;
		throw;
	}
}

void xxxxRecord::initReferenceTime() {
	refTimePoint=high_resolution_clock::now();
}

void xxxxRecord::updateTimeStamp() {
	high_resolution_clock::time_point tNow=high_resolution_clock::now();
	duration<double, std::milli> timeSpan = tNow - refTimePoint;
	timestamp=(unsigned long) timeSpan.count();
}

void xxxxRecord::printCalibrationDifferences(const xxxxRecord &other) const {
	cout << "Absolute differences (Raw-Final):" << endl;
	cout << "  Accel: ";
	::printDifferences(IMU_AccelRaw, other.IMU_AccelRaw); cout << ' ';
	::printDifferences(IMU_Accel, other.IMU_Accel); cout << ' ' << endl;

	cout << "  Mag: ";
	::printDifferences(magRaw, other.magRaw); cout << ' ';
#ifdef INCLUDE_MAG_RAW_CALIBRATED
	::printDifferences(magCalib, other.magCalib); cout << ' ';
#endif
	::printDifferences(mag, other.mag); cout << endl;

	cout << "  Gyro: ";
	::printDifferences(gyroRaw, other.gyroRaw); cout << ' ';
	::printDifferences(gyro, other.gyro); cout << ' ' << endl;
}

ostream& operator<<(ostream& os, const xxxxRecord & record) {
	record.printCSV(os);
	return os;
}

istream& operator>>(istream& is, xxxxRecord & record) {
	record.readFromCSV(is);
	return is;
}
