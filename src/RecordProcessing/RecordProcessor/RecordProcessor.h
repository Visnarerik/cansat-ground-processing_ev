/*
 * RecordProcessor.h
 */

#pragma once


#include <RecordProcessor/Processor.h>
#include <exception>
#include <sstream>
#include <type_traits>
#include "CSV_Record.h"
#include "DebugUtils.h"

// Temporary fix (see method forwardResultToNextProcessor() for details
#include "xxxxRecord.h"
#include "xxxxGroundRecord.h"

/** @brief An base class for processors accepting and producing CSV_Records
 *
 *  This is the base class of every processor handling CSV_Records. Any subclass should:
 *    -# Override method #doProcess(const RECORD_IN& recordIn, RECORD_OUT& recordOut) to perform the
 *       actual calculation. In case the input record is not modified, it should be copied into recordOut.
 *    -# Possibly override doPrepare() and/or doTerminate(), if they have any
 *    preparation to do just before the first call to doProcess(), or clean-up to do just after the
 *    last record is processed.
 *
 *  Although RecordProcessors work on CSV_Records, they can be fed with strings (which will be parsed
 *  into the appropriate CSV_Record) and they can be queried for result in string format (the output
 *  CSV_Record will be converted into a string representation.
 *
 *  The template arguments are required to be non-abstract subclasses of CSV_Record.
 *
 *  @warning See issue in #forwardResultToNextProcessor.
 *
 */
template<class RECORD_IN, class RECORD_OUT>
class RecordProcessor : public Processor {
	static_assert(!std::is_abstract<RECORD_IN>::value, "RECORD_IN is an abstract class");
	static_assert(!std::is_abstract<RECORD_OUT>::value, "RECORD_OUT is an abstract class");
	static_assert(std::is_base_of<CSV_Record, RECORD_IN>::value, "RECORD_IN is not derived from CSV_Record");
	static_assert(std::is_base_of<CSV_Record, RECORD_OUT>::value, "RECORD_OUT is not derived from CSV_Record");
public:
	/** Constructor.
	 *  @param fb	 If withVisualFeedback, feedback on cout is allowed during error-less processing.
	 *               If noVisualFeedback, feedback on cout in only allowed in error cases.
	 */
	RecordProcessor(VisualFeedback_t fb=withVisualFeedback) : Processor(fb) { DBG_FCT};

	virtual ~RecordProcessor() {};

	/** Process the provided record, received as a CSV_Record. This is the public method to call
	 *  to process a record.
	 * 	@param record The record to process. It is assumed NOT to be terminated by a '\n'.
	 * 	@return true if processing was successful, false otherwise.
	 */
    virtual bool processRecord(const RECORD_IN& record);

    /** Obtain a pointer to a CSV_Record containing the result of the processing
      *  of this process.
      *  @return A pointer to the CSV_Record resulting from the processing
      */
     virtual const RECORD_OUT * getSpecializedResultRecord() { return &resultRecord; };

     // Override base class method.
     virtual const CSV_Record* getLocalResultRecord() { return &resultRecord; };

    /** Obtain a String containing the result of the processing (overriden)
     *  @return The string resulting from the processing.
     */
    virtual std::string getLocalResult();

protected:
     /** @name 1. Methods that could require overloading
     *  Those methods require overloading (not all of them for each processor, see class
     *  documentation for details).
     *  @{
     */

     /** Override this method to perform any preparation that cannot be performed
      *  in the constructor (optional). It is called just before the first call to
      *  of the doProcess() methods.
      *  @return true if preparation was successful and the processor is ready
      *          to accept records, false otherwise.
      */
     virtual bool doPrepare() { DBG_FCT ; return true; };

	/** Process the provided record, received as a CSV_Record. The default implementation
	 *  just copies recordOut to recordIn.
	 *  @param recordIn The record to process.
	 *  @param recordOut The data resulting from the processing.
	 *  				 If the processing does not change the data, recordIn should be copied
	 *  				 to recordOut.
	 * 	@return true if processing was successful, false otherwise.
	 */
    virtual bool doProcess(const RECORD_IN& recordIn , RECORD_OUT& recordOut) {
    	recordOut=recordIn;
    	return true;
    };

	/** Override this method to perform any action required as soon as possible after the last
	 *  record is processed and should preferably not wait until the destructor is called (optional)
	 *  This method will be called at the latest in the destructor, but a well behaved user could
	 *  have it called earlier (RecordSource does). This method MUST support multiple calls.
	 *  Default version is empty.
	 */
    virtual void doTerminate() {};

    /** @} */ // End of group of methods that could require overloading

    //----------------------------------------------------------------------------------------------------

    /** @name 2. Methods that should normally not require overloading
     *  In the standard usage of this class, the following methods should not require
     *  overloading.
     *  @{
     */

	/** Process the provided record, received as a String.
	 * @param  recordString The record to process, in string format.
	 * 		   It is assumed NOT to be terminated by a '\n'.
	 * @return true if processing was successful, false otherwise.
	 */
    virtual bool doProcess(const std::string& recordString);

	/** Forward the results of the processing to the next processor, if any.
	 *  @return true if everything ok, false otherwise.
	 */
	virtual bool forwardResultToNextProcessor();
    /** @} */ // End of group of methods that should not be overloaded.

#ifdef CURRENTLY_NOT_USED
	//See comment in methods forwardToNextProcessor().
	/** Template methods to define whether this template specialisation accepts class RECORD
	 *  as input records. Returns false.
	 */
	template<class RECORD>
	struct acceptsRecords : std::false_type {};
	/** Template specialisation which returns true if RECORD is the same class as RECORD_IN */
	template<RECORD_IN>
	struct acceptsRecords<RECORD_IN> : std::true_type {} ;
#endif

private:
    RECORD_OUT		resultRecord;   /**< The result of the processing */
    static constexpr auto DBG=false;
};

template <class RECORD_IN, class RECORD_OUT>
bool RecordProcessor<RECORD_IN, RECORD_OUT>::doProcess(const std::string& stringRecordIn) {
	DBG_FCT
	// We need a CSV_Record with the correct type to parse the string.
	RECORD_IN in;
	in.loadFromCSV_String(stringRecordIn);
	return  doProcess(in, resultRecord);
}

template <class RECORD_IN, class RECORD_OUT>
bool RecordProcessor<RECORD_IN,RECORD_OUT>::forwardResultToNextProcessor() {
	DBG_FCT
	bool result;
	if (getNextProcessor() != nullptr) {

		/** @warning Issue with the current version:
		 * 		  We need to know whether the next processor accepts the CVS_Record subclass  we
		 *        have as output (which is from class RECORD_OUT).
		 * @warning
		 *        We could easily detect whether it is a subclass of RecordProcessor, by using
		 *        a RecordProcessorBase baseclass for RecordProcessor<RECORD_IN, RECORD_OUT>
		 *        and further test whether dynamic_cast<RecordProcessorBase*>(getNextProcessor());
		 *        is null of not.
		 *        If not null, the next processor IS a RecordProcessor(X,Y), and we should test
		 *        whether X == RECORD_OUT.  But how can we do that? This is a runtime-check, not a
		 *        compile_time one, and even SFINAE expressions seem not to do the trick.
		 * @warning TEMPORARY SOLUTION (NOOOOOT CLEAN!):
		 *  	  Test only for RecordProcess<RECORD_OUT, xxxxRecord>
		 *  	          and RecordProcess<RECORD_OUT, xxxxGroundRecord>
		 *  	  If the next processor is not one of those, it will be fed with a string instead of
		 *  	  the RECORD_OUT, and will have to parse it, if it is not string based.
		 *  	  This is not a clean solution at all, and will cause unnecessary parsing for any
		 *  	  processor of tyme RecordProcessor<RECORD_OUT,xxx> if xxx is neither xxxxRecord or
		 *  	  xxxxGroundRecord...
		 *  	  Any hints?
		 */
		typedef RecordProcessor<RECORD_OUT, xxxxRecord> RP;
		RP* rp1=dynamic_cast<RP*>(getNextProcessor());
		if (rp1) {
			LOG_IF(DBG,DEBUG) << "Forwarding CSV_Record to next processor (out=xxxx)";
			result = rp1->processRecord(resultRecord);
		}
		else
		{
			typedef RecordProcessor<RECORD_OUT, xxxxGroundRecord> RP_Ground;
			RP_Ground* rp2=dynamic_cast<RP_Ground*>(getNextProcessor());
			if (rp2) {
				LOG_IF(DBG,DEBUG) << "Forwarding CSV_Record to next processor (out=xxxxGround)";
				result = rp2->processRecord(resultRecord);

			}
			else
			{
				std::ostringstream str;
				resultRecord.printCSV(str);
				LOG_IF(DBG, DEBUG) << "Forwarding CSV string version of record '" << str.str() << "'";
				result = getNextProcessor()->processString(str.str());
			}
		}
	} // nextProcessor
	else result=true;
	return result;
}

template <class RECORD_IN, class RECORD_OUT>
bool RecordProcessor<RECORD_IN,RECORD_OUT>::processRecord(const RECORD_IN& CSV_RecordIn) {
	DBG_FCT
	bool result=false;
	if (!Prepare()) return false;
	try {
		result=doProcess(CSV_RecordIn, resultRecord);
		// Forward to next processor
		if (result) result = forwardResultToNextProcessor();

	} catch (std::exception &e) {
		// Since the process should never stop processing the next records because this one
		// failed, we swallow possible exceptions.
		std::cerr << "RecordProcessor::process(RECORD_IN): Exception: " <<e.what() << " (swallowed)" << std::endl;
 	}
	return result;
}

template <class RECORD_IN, class RECORD_OUT>
std::string RecordProcessor<RECORD_IN,RECORD_OUT>::getLocalResult() {
	DBG_FCT
	std::ostringstream str;
	resultRecord.printCSV(str);
	LOG_IF(DBG, DEBUG) << "Returning CSV string version of record '" << str.str() << "'";
	return str.str(); /* This causes a copy, the stream can be deleted */
}



