/*
 * StringProcessor.h
 *
 */

#pragma once
#include "RecordProcessor/Processor.h"
#include "DebugUtils.h"

/** @brief A transparent string processor (which just outputs its input), to be used as base class by
 *  any string processor.
 *
 *  StringProcessor is the base class for any processor that performs string-based processing.
 *  Such a processor should:
 *    -# Override method #doProcess(const std::string&, std::string&).
 *    -# Possibly override methods #doPrepare #doTerminate.
 *
 */
class StringProcessor : public Processor {
public:
	StringProcessor(Processor::VisualFeedback_t fb=Processor::withVisualFeedback) : Processor(fb) {} ;
	virtual ~StringProcessor() {};

    virtual std::string getLocalResult() { return resultString; }

protected:
    /** @name 1. Methods that could require overriding
     *  Those methods require overriding (not all of them for each processor, see class
     *  documentation for detail
     *  @{
     */

    /** Overload this method to perform any preparation that cannot be performed
     *  in the constructor (optional). It is called just before the first call to
     *  of the doProcess() methods.
     *  @return true if preparation was successful and the processor is ready
     *          to accept records, false otherwise.
     */
    virtual bool doPrepare() { DBG_FCT ; return true; };

    /** Process the provided string, generating result as string as well.
      *  This default implementation just copies stringIn to stringOut.
      * @param stringIn  The string to process. It is assumed NOT to be terminated by
      *                  a '\\n'.
      * @param stringOut The data resulting from the processing, in string format, without a final '\\n'.
      * If the processing does not change the data, stringIn should be copied to stringOut.
      * @return true if processing was successful, false otherwise.
      */
     virtual bool doProcess(const std::string& stringIn, std::string& stringOut) {
     	stringOut=stringIn;
     	return true;};

    /** Overload this method to perform any action required as soon as possible after the last
     *  record is processed and should preferably not wait until the destructor is called (optional)
     *  This method will be called at the latest in the destructor, but a well behaved user could
     *  have it called earlier (RecordSource does). This method MUST support multiple calls.
     *  Default version is empty.
     */
    virtual void doTerminate() {};

    /** @} */ // End of group of methods that could require overriding
    //---------------------------------------------------------------------------------------------
    /** @name 2. Methods that should not require overriding
     *  @{
     */
    bool doProcess(const std::string& stringRecordIn) {
     	DBG_FCT
     	return doProcess(stringRecordIn, resultString);
     };
    /** @} */

private:
    std::string		resultString{};    /**< The result of the processing as a string. */
    static constexpr auto DBG=false;
};



