/*
 * RP_RecordDelay.h
 */

#pragma once
#include "RecordProcessor.h"
#include <string>
#include <chrono>
#include <thread>

#include "xxxxRecord.h"
#include "DebugUtils.h"


/** @brief A record processor which just delays the transmission of the record according
 *  to the timestamp field. It is intended to be used after a CSV_RecordSource, to
 *  simulate the real time processing of the records.
 */
template<class RECORD>
class RP_RecordDelay : public RecordProcessor<RECORD, RECORD> {
	static_assert(std::is_base_of<xxxxRecord, RECORD>::value, "RECORD is not derived from xxxxRecord");
	using  clock  = std::chrono::high_resolution_clock;
	using time_point_type   = std::chrono::time_point< std::chrono::high_resolution_clock> ;

public:
	/** Constructor
	 */
	RP_RecordDelay(): RecordProcessor<RECORD,RECORD>() { lastRecordTimestamp=0;};

	virtual ~RP_RecordDelay() {};

	/** Actually perform the processing, which is just waiting for the right duration since
	 *  the previous record was processed, baised on the records timestamp.
	 * @param recordIn  The record to process.
	 * @param recordOut The record resulting from the processing (always identical).
	 * @return Always true (cannot possibly fail).
	 */
	virtual bool doProcess(const RECORD& recordIn, RECORD& recordOut)
	{
		DBG_FCT
		recordOut = recordIn;

		// Wait the appropriate delay since the previous record.
		if (lastRecordTimestamp !=0) {
			unsigned long delay = recordIn.timestamp-lastRecordTimestamp;

			this_thread::sleep_for(	lastProcessingTime
					+chrono::milliseconds(delay) - clock::now());
		}
		lastProcessingTime=clock::now();
		lastRecordTimestamp=recordIn.timestamp;
		return true;
	};

private:
	 time_point_type lastProcessingTime;
	 unsigned long lastRecordTimestamp;
	 static constexpr auto DBG=false;
};


