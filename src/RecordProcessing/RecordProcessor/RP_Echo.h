/*
 * RP_Echo.h
 */

#pragma once
#include <RecordProcessor/StringProcessor.h>
#include "RecordProcessor/RecordProcessor.h"
#include <ostream>
#include <string>

/** A simple record processor which just echoes the received record on an output stream. */
template<class RECORD>
class RP_RecordEcho : public RecordProcessor<RECORD, RECORD> {
public:
	/** Constructor
	 *  @param outputStream The ostream on which record must be echoed
	 */
	RP_RecordEcho(std::ostream &outputStream) : RecordProcessor<RECORD,RECORD>(), os(outputStream) {};

	/** Just echo the provided record on the output stream provided in the constructor.
	 * @param recordIn  The record to process. It is assumed NOT to be terminated by a '\\n'.
	 * @param recordOut The record resulting from the process: it is set to recordIn.
	 * @return Always true.
	 */
	bool doProcess(const RECORD& recordIn, RECORD& recordOut) {
		os << recordIn << std::endl;
		recordOut=recordIn;
		return true;};
private:
	std::ostream& os;
};

/** A simple record processor which just echoes the received string on an output stream. */
class RP_StringEcho : public StringProcessor {
public:
	/** Constructor
	 *  @param outputStream The ostream on which record must be echoed
	 */
	RP_StringEcho(std::ostream &outputStream) : StringProcessor(), os(outputStream) {};

	/** Just echo the provided record on the output stream provided in the constructor.
	 * @param recordIn  The record to process. It is assumed NOT to be terminated by a '\\n'.
	 * @param recordOut The record resulting from the process: it is set to recordIn.
	 * @return Always true.
	 */
	bool doProcess(const std::string& in, std::string& out) {
		os << in << std::endl;
		out=in;
		return true;};
private:
	std::ostream& os;
};
