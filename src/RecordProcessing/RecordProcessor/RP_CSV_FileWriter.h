/*
 * RP_CSV_FileWriter.h
 *
 */

#pragma once

#include <RecordProcessor/RecordProcessor.h>

/** @brief A processor to store CSV_Records into a text file. */
template<class RECORD>
class RP_CSV_FileWriter: public RecordProcessor<RECORD,RECORD> {
public:
	/** Constructor.
	 *  @param filePath The full path to the output file. If the file does
	 *                  not exist, it is created.
	 *  @param append	If true, and the file exists, content is appended
	 *
	 */
	RP_CSV_FileWriter(	std::string filePath,
						bool append=false) :
	   RecordProcessor<RECORD, RECORD>(),
	   filePath(filePath) {
		os.exceptions(std::ifstream::failbit | std::ifstream::badbit);
		if (append) {
			os.open(filePath, std::ofstream::app);
			cout << "Appending to file '" << filePath << "'" << endl;
		}
		else {
			os.open(filePath);
			cout << "Created file '" << filePath << "'" << endl;
		}
		RECORD rec;
		if (!append) rec.printCSV_Header(os);// no final endl!
	};
	~RP_CSV_FileWriter() { os.close();};
	void printProcessorDescription(std::ostream & os) {
		    	os << "RP_CSV_FileWriter(file=" ;
		    	os << filePath << ")";
		}
protected:
	/** Just echo the provided record on the ostream provided in the constructor.
	 *  @param recordIn  The record to process. It is assumed NOT to be terminated by a '\\n'.
	 *  @param recordOut The record resulting from the process: it is set to recordIn.
	 *  @return Always true.
	 */
	bool doProcess(const RECORD& recordIn, RECORD& recordOut) {
		os << std::endl << recordIn;
		recordOut=recordIn;
		return true;
	};

	/** Just echo the provided record on the ostream provided in the constructor.
	 *  @param recordIn  The record to process. It is assumed NOT to be terminated by a '\\n'.
	 *  @param recordOut The record resulting from the process: it is set to recordIn.
	 *  @return Always true.
	 */
	bool doProcess(const std::string& recordIn, std::string& recordOut) {
		os << std::endl << recordIn;
		recordOut=recordIn;
		return true;
	};
private:
	std::ofstream os;
	const std::string filePath;
};

