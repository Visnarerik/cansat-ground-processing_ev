/*
 * RecordProcessorBase.cpp
 *
 */

#include <RecordProcessor/Processor.h>

Processor::Processor(VisualFeedback_t fb) 	:
		nextProcessor(nullptr),
		lastProcessor(this),
		useVisualFeedback(fb==withVisualFeedback),
		prepared(false){
	DBG_FCT
}

Processor::~Processor() {
	doTerminate(); // Terminate just this processor, not the whole chain here (other will be terminated
				   // by their own destructor.
}

bool Processor::processString(const std::string& aString) {
    	DBG_FCT
     	bool result=false;
     	if (!Prepare()) return false; // Error msg is in base class.
     	try {
     		result=doProcess(aString);
     		if (result) {
     			result=forwardResultToNextProcessor();
     		}
     	} catch (std::exception &e) {
     		// Since the process should never stop processing the next records because this one
     		// failed, we swallow possible exceptions.
     		std::cerr << "Processor::process: Exception: " <<e.what() << " (swallowed)" << std::endl;
     	}
     	return result;;
 }

void Processor::terminateProcessing() {
	DBG_FCT
	doTerminate();
	if (nextProcessor) nextProcessor->terminateProcessing();
};

void Processor::appendProcessor(Processor& processor) {
	DBG_FCT
    if (&processor == this) {
    	throw std::runtime_error("Error: attempt to append processor to itself");
    }
	// check no processor is in the chain already to avoid cycles
	// (the appended one could already have next ones)
	Processor *current=this;
	while (current) {
		Processor *newP=&processor;
		while (newP) {
			if (newP==current) {
				throw std::runtime_error("Error: attempt to append processor twice or to create a cycle");
			}
			newP=newP->nextProcessor;
		}
		 current=current->nextProcessor;
	}

	// Proceed with append:
	// update nextProcessor in the previously last one,
	// update lastProcessor in the whole chain (including the newly added ones).
	lastProcessor->nextProcessor=&processor;
	current=this;

	while (current) {
		current->lastProcessor=processor.lastProcessor;
		current=current->nextProcessor;
	}
}

bool Processor::Prepare() {
	if (prepared) return true;
	else{
		prepared=doPrepare();
		if (!prepared) std::cout << "*** Error in processor's doPrepare(). Aborted." << std::endl;
		return prepared;
	}
};
bool Processor::forwardResultToNextProcessor() {
	DBG_FCT
	bool result;
	if (nextProcessor != nullptr) {
		LOG_IF(DBG,DEBUG) << "Forwarding string '" << getLocalResult() << "' to next processor";
		result = nextProcessor->processString(getLocalResult());
	} // nextProcessor
	else result=true;
	return result;
}

void Processor::printProcessorChainDescription(std::ostream & os) {
	Processor* current = this;
	int counter = 1;
	while (current) {
		os << counter++ << ". " ;
		current->printProcessorDescription(os);
		os << std::endl;
		current=current->nextProcessor;
	}
}

void Processor::deleteAppendedProcessors() {
	DBG_FCT
	Processor *current=nextProcessor;
	while (current) {
		Processor *tmp = current;
		current=current->nextProcessor;
		delete tmp;
	}
	nextProcessor=nullptr;
};
