/*
 * xxxxGroundRecord.h
 *
 */

#pragma once

#include "xxxxRecord.h"

class xxxxGroundRecord: public xxxxRecord {
public:
	xxxxGroundRecord();
	/** Operator= overloading to be able to initialize the xxxxRecord part of the object */
	xxxxGroundRecord& operator=(const xxxxRecord& other);
	xxxxGroundRecord(const xxxxGroundRecord&)=delete;
	virtual ~xxxxGroundRecord();

	// Public data members carried by the record, in addition to base class data
	float GPS_PositionX{}, GPS_PositionY{}, GPS_PositionZ{}; /**<  in m, in ??? referential */
	float GPS_VelocityX{}, GPS_VelocityY{}, GPS_VelocityZ{}; /**<  in m/s, in ??? referential */

	// Output of Kalman filter
	float positionX{}, positionY{}, positionZ{};
	float velocityX{}, velocityY{}, velocityZ{};


	virtual void clear();
	virtual void printCSV_Header(ostream &os) const;
	virtual void printCSV(ostream &os) const;
protected:
	virtual unsigned int parseRow(const CSV_Row &row);

};

