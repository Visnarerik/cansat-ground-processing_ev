//============================================================================
// Name        : GroundProcessingMain.cpp
// Author      : CSPU: Cansat 2019 (IsaTwo)
// Version     :
// Copyright   : Collège Saint-Pierre, Uccle
// Description : TO BE COMPLETED.
//============================================================================

#include <iostream>
#include "DebugUtils.h"
constexpr auto DBG = true;

INITIALIZE_EASYLOGGINGPP

using namespace std;

int main(int argc, char* argv[]) {
	START_EASYLOGGINGPP(argc, argv);
	configureLogging();

	LOG_IF(DBG, DEBUG) << "Launching GroundProcessing..." << endl;
	cout << "Hi" << endl; // Temporary prints Hi

	return 0;
}
